---
layout: post
title: Cantor in KDE Edu Sprint 2017
date: 2017-10-22
---

KDE Edu Sprint 2017 was held in Berlin, from 7th to 9th October. During this meeting, Cantor developers Filipe Saraiva and Rishabh Gupta worked in some tasks and the results will be released soon.

Please, read the reports of [Filipe Saraiva](http://blog.filipesaraiva.info/?p=1885) and [Rishabh Gupta](https://rish9511.wordpress.com/2017/10/22/kde-edu-sprint-2017/) for more information. A [KDE Edu Sprint 2017 report](https://dot.kde.org/2017/11/01/2017-kde-edu-sprint) is also available in KDE website.
