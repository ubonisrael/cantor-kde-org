---
layout: post
title: Cantor in GSoC 2019 - Support to Jupyter notebooks
date: 2019-06-09
---

This year Cantor has a slot in Google Summer of Code program. The developer Nikita Sirgienko, mentored by Alexander Semke, is working to provide support to Jupyter notebooks in Cantor.

The idea is provide a way to import/export Jupyter notebooks to/from Cantor. It could improve the Cantor userbase and popularity if the software can manage the main notebook format available at the moment.
    
You can read Nikita's proposal in [GSoC 2019 page](https://summerofcode.withgoogle.com/projects/#4583302302793728) and follow the news in the [project website](https://sirgienkogsoc2019.blogspot.com/).
